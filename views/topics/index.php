<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\topics\TopicsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Topics List');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="topics-record-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Topic'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

	<?php //echo '<pre>' ; var_dump($searchModel); echo '</pre>'; exit; ?> 

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            [
            	'attribute' => 'title',
            	'format' => 'html',
            	'value' => function($data){ return Html::a($data->title, ['topics/view', 'id' => $data->id] ); },
            ],
            //'content:ntext',
            [
           		'attribute' => 'latest_answer',
            	'filter' => false
	        ],
			[
        		'attribute' => 'answer_count',
        		'filter' => false
			],
        	[
        		'attribute' => 'is_api',
        		'value' => function($data){ return $data->is_api ? 'Yes' : 'No'; },
        		'filter' => false
    		],
            // 'created_at',
            [
            	'attribute' => 'created_by',
            	'value' => function($data){ return $data['createdBy']->username; },
            	'filter' => false        
            ],
            [
            		'class' => 'yii\grid\ActionColumn',
            		'template' => '{view}'
    		],
        ],
    ]); ?>

</div>
