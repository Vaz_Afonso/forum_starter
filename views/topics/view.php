<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\topics\TopicsRecord */
/* @var $reply app\models\replies\RepliesRecord */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Topics Records'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="topics-record-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php ////echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= 
    			
    			Html::button( Yii::t('app', 'Reply'), [
	            	'class' => 'btn btn-success modal_button',
    				'value' => Url::to(['replies/create', 'topic' => $model->id])
    			]);
    		?>
        <?php /* echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) */?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'content:ntext',
            'latest_answer',
            'answer_count',
            [
            	'attribute' => 'is_api',
            	'value' => $model->is_api ? 'Yes' : 'No',
    		],
            'created_at',
            [
            	'attribute' => 'created_by',
            	'format'=> 'raw',
            	'value' => $author->username,
            ],
        ],
    ]) ?>
    
    <h3>Replies</h3>
    
    <!-- Replies Section -->
    
    <div class="container">
    	<div class="row">
    	<?php Pjax::begin(['id' => 'replies_container']);?>
    	<?php if( count($replies) < 1 ): ?>
    		
    		<p><?= Yii::t('app', 'No messages yet.'); ?></p>
    		
    		<?= 
    			
    			Html::button( Yii::t('app', 'Be the first to reply to this'), [
	            	'class' => 'btn btn-default modal_button',
    				'value' => Url::to(['replies/create', 'topic' => $model->id])
    			]);
    		?>
    	
    	<?php else:?>
		    <?php foreach( $replies as $r ): ?>
		    	
		    	<div class="panel panel-default">
					<div class="panel-heading">
						
						<strong><?= $r->title; ?></strong>
						<small>
						<?= 
							$r->getCreatedBy()->one()->username .' '.
							Yii::t('app', 'on') .' '. $r->created_at .' '.
							Yii::t('app', 'via') .' '. ($r->is_api ? 'Api' : 'Web') 
						?>
						</small>
						
					</div>
			  		<div class="panel-body">
			    		<?= $r->content; ?>
			  		</div>
				</div>
		    	
		    <?php endforeach;?>
		    
		
		<?php endif;?>
		<?php Pjax::end();?>
		
		<?php 
		/* 
		**
		* Modal form for replying to topic
		**
		*/
			Modal::begin([
					'header' => 'Place your reply',
					'id' => 'modal',
					'size' => 'modal-md'
			]);
		?>
		
		<div id="modal_content" data-topic="<?= $model->id; ?>"></div>
		
		<?php Modal::end()?>
		
		</div>
    </div>
</div>
