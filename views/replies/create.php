<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\replies\RepliesRecord */

$this->title = Yii::t('app', 'Place your Reply');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Replies Records'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="replies-record-create">

    <!-- <h1><?php //echo Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

