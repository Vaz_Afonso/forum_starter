<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\replies\RepliesRecord */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="replies-record-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>
    
    <?= $form->field($model, 'topic_id')->hiddenInput()->label(false) ?>
<!-- 
    <?= $form->field($model, 'is_api')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>
-->
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


<?php 

/* TODO: nva refresh the article content as well */

/* Modal */
$script = <<< JS

console.log('sdsdfsdf');
		
$('#repliesrecord-topic_id').val( $('#modal_content').attr('data-topic') );

$('form').on('submit', function(e){
	 	
		e.preventDefault();
		
		var \$form = $(this);
		$.post(
			\$form.attr('action'),
			\$form.serialize()
		)
			.done(function(response){
				
				if( response == 1 ){
					console.log(response);
					$(\$form).trigger("reset");
					$('#modal').modal('toggle');
					$.pjax.reload({container: '#replies_container'});
				} else{
					console.log("unable to save reply");	
				}
			
			}).fail(function(){
				console.log("server error");	
			});
	
		return false;
});

JS;

$this->registerJs($script);

?>

</div>
