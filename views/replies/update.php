<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\replies\RepliesRecord */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Replies Record',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Replies Records'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="replies-record-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
