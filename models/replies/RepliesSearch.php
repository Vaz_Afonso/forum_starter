<?php

namespace app\models\replies;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\replies\RepliesRecord;

/**
 * RepliesSearch represents the model behind the search form about `app\models\replies\RepliesRecord`.
 */
class RepliesSearch extends RepliesRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_api', 'topic_id', 'created_by'], 'integer'],
            [['title', 'content', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RepliesRecord::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_api' => $this->is_api,
            'created_at' => $this->created_at,
            'topic_id' => $this->topic_id,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
