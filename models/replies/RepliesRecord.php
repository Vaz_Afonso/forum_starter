<?php

namespace app\models\replies;

use Yii;
use app\models\topics\TopicsRecord;
use app\models\users\UsersRecord;

/* @var $topic app\models\topics\TopicsRecord */

/**
 * This is the model class for table "replies".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $is_api
 * @property string $created_at
 * @property integer $topic_id
 * @property integer $created_by
 *
 * @property Users $createdBy
 * @property Topics $topic
 */
class RepliesRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'replies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content', 'topic_id', 'created_by'], 'required'],
            [['content'], 'string'],
            [['is_api', 'topic_id', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'is_api' => Yii::t('app', 'Is Api'),
            'created_at' => Yii::t('app', 'Created At'),
            'topic_id' => Yii::t('app', 'Topic ID'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(UsersRecord::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic()
    {
        return $this->hasOne(Topics::className(), ['id' => 'topic_id']);
    }

    /**
     * @inheritdoc
     * @return RepliesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RepliesQuery(get_called_class());
    }
    
    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
    
    	if(parent::beforeValidate()) {
    
    		if( $this->isNewRecord ){
    			
    			//TODO : nva $_post method
    			if( Yii::$app->request->get('k') !== null ){
					$user = UsersRecord::findIdentityByAccessToken(Yii::$app->request->get('k'));
					$this->created_by = $user->id;
				} else{
					$this->created_by = \Yii::$app->user->identity->id;
				}
				
    		}
    			
    		return true;
    	} else {
    
    		return false;
    	}
    }
    
    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
    	parent::afterSave($insert, $changedAttributes);
    	
    	// change topic last reply
    	$this->updateRelativeTopic($this->topic_id);
    	 
    }
    
    private function updateRelativeTopic($topic_id)
    {
    	$topic = TopicsRecord::findOne(['id' => $topic_id]);
    	$topic->latest_answer = Yii::$app->formatter->asDatetime(time(), 'php:Y-m-d H:i:s');
    	$topic->answer_count = $topic->getReplies()->count();
    	$topic->save();
    }
    
}
