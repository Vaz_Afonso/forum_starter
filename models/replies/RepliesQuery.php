<?php

namespace app\models\replies;

/**
 * This is the ActiveQuery class for [[RepliesRecord]].
 *
 * @see RepliesRecord
 */
class RepliesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return RepliesRecord[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RepliesRecord|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}