<?php

namespace app\models\topics;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\topics\TopicsRecord;

/**
 * TopicsSearch represents the model behind the search form about `app\models\topics\TopicsRecord`.
 */
class TopicsSearch extends TopicsRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'answer_count', 'is_api', 'created_by'], 'integer'],
            [['title', 'content', 'latest_answer', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TopicsRecord::find()->joinWith('createdBy', true, 'left join');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'latest_answer' => $this->latest_answer,
            //'answer_count' => $this->answer_count,
            //'is_api' => $this->is_api,
            'created_at' => $this->created_at,
            //'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
