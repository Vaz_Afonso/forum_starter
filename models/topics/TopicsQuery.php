<?php

namespace app\models\topics;

/**
 * This is the ActiveQuery class for [[TopicsRecord]].
 *
 * @see TopicsRecord
 */
class TopicsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TopicsRecord[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TopicsRecord|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}