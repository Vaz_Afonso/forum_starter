<?php

namespace app\models\topics;


use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\models\replies\RepliesRecord;
use app\models\users\UsersRecord;

/**
 * This is the model class for table "topics".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $latest_answer
 * @property integer $answer_count
 * @property integer $is_api
 * @property string $created_at
 * @property integer $created_by
 *
 * @property Replies[] $replies
 * @property Users $createdBy
 */
class TopicsRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'topics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content', 'created_by'], 'required'],
            [['content'], 'string'],
            [['latest_answer', 'created_at'], 'safe'],
            [['answer_count', 'is_api', 'created_by'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'latest_answer' => Yii::t('app', 'Latest Answer'),
            'answer_count' => Yii::t('app', 'Answer Count'),
            'is_api' => Yii::t('app', 'Is Api'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
    
    public function behaviors()
    {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'created_at',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReplies()
    {
        return $this->hasMany(RepliesRecord::className(), ['topic_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReplyById($reply_id)
    {
    	return $this->hasOne(RepliesRecord::className(), ['topic_id' => 'id', 'id' => $reply_id]);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(UsersRecord::className(), ['id' => 'created_by']);
    }

    /**
     * @inheritdoc
     * @return TopicsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TopicsQuery(get_called_class());
    }
    
	/**
	 * @inheritdoc
	 */
	public function beforeValidate()
	{	
		
		if(parent::beforeValidate()) {
	    	
			if( $this->isNewRecord ){
				
				//TODO : nva $_post method
				if( Yii::$app->request->get('k') !== null ){
					$user = UsersRecord::findIdentityByAccessToken(Yii::$app->request->get('k'));
					$this->created_by = $user->id;
				} else{
					$this->created_by = \Yii::$app->user->identity->id;
				}
					
				$this->answer_count = 0;
				$this->latest_answer = null;
			}
			
	        return true;
	    } else {
	    	
	        return false;
	    }  
	}
}
