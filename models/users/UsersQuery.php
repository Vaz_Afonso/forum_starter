<?php

namespace app\models\users;

/**
 * This is the ActiveQuery class for [[UsersRecord]].
 *
 * @see UsersRecord
 */
class UsersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return UsersRecord[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UsersRecord|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}