<?php

namespace app\models\users;

use Yii;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property string $created_at
 *
 * @property Replies[] $replies
 * @property Topics[] $topics
 */
class UsersRecord extends ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['created_at', 'auth_key'], 'safe'],
            [['username'], 'string', 'max' => 75],
            [['password'], 'string', 'max' => 60],
            [['auth_key'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @inheritdoc
     * @return UsersQuery the active query used by this AR class.
     */
    public static function find()
    {
    	return new UsersQuery(get_called_class());
    }
    
    /*
     * Before save create password hash
     *
     * @see \yii\db\BaseActiveRecord::beforeSave($insert)
     */
    public function beforeSave($insert)
    {
    	$return = parent::beforeSave($insert);
    	 
    	$this->password = Yii::$app->security->generatePasswordHash($this->password);
    	$this->auth_key = $this->generateAuthKey();
    	 
    	if( $this->isNewRecord ){
    		$this->created_at = $this->created_at = Yii::$app->formatter->asTimestamp(time());
    	}
    	 
    	return $return;
    }
    
    public function beforeValidate(){
    	return parent::beforeValidate();
    }
    
    
    public static function findIdentity($id)
    {
    	return static::findOne($id);
    }
    
    public static function findByUsername($username)
    {
    	return static::findOne(['username' => $username]);
    }
    
    public static function findIdentityByAccessToken($token, $type = null)
    {
    	return static::findOne(['auth_key' => $token]);
    }
    
    /*public static function findIdentityByAccessToken($token, $type = null)
    {
    	return static::findOne(['access_token' => $token]);
    }*/
    
    public function getId()
    {
    	return $this->id;
    }
    
    public function getAuthKey()
    {
    	return $this->auth_key;
    }
    
    public function validateAuthKey($auth_key)
    {
    	return $this->auth_key === $auth_key;
    }
    
    /*
     * validate password hash
     * @return boolean
     */
    public function validatePassword($password)
    {
    	 
    	if( Yii::$app->getSecurity()->validatePassword($password, $this->password) )
    		return true;
    	 
    	return false;
    }
    
    /**
     * @return string(32) a random key for authKey authentication
     */
    public static function generateAuthKey()
    {
    	$key = bin2hex(openssl_random_pseudo_bytes(16));
    	 
    	return $key;
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReplies()
    {
        return $this->hasMany(Replies::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopics()
    {
        return $this->hasMany(Topics::className(), ['created_by' => 'id']);
    }
    
    
}
