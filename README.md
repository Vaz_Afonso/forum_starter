- Install composer on your local environment  

- Install Yii2 basic template - instructions in the link below    
http://www.yiiframework.com/doc-2.0/guide-start-installation.html
  
- Download the zip folder from this repository  

- Copy the following folders and replace them on your directory:  
./assets/    
./config/  
./controllers/   
./migrations/  
./models/  
./vendor/yiisoft/yii2/rest/  
./views/  
./web/css/  
./web/js/  
  
- Copy the .htaccess file into your /web/ folder   

- Create a MySQL database for this project and configure the /config/db.php   -> dsn, username and password;  
  
- Configure your server's permissions ( chmod )    

- Run the migrations using the command line. Go to the project root and run the command ./yii migrate.  