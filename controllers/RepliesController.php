<?php

namespace app\controllers;

use Yii;
use app\models\replies\RepliesRecord;
use app\models\replies\RepliesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\topics\TopicsRecord;

/**
 * RepliesController implements the CRUD actions for RepliesRecord model.
 */
class RepliesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RepliesRecord models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RepliesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RepliesRecord model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    /**
     * Creates a new Reply to an existing Topic.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $topic_id 
     * @return mixed
     */
    public function actionCreate( $topic)
    {
    	//check if topic exists
    	if( TopicsRecord::find()->where([ 'id' => $topic ])->exists() ){
    
	    	$model = new RepliesRecord();
	        
	        
	        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	            return true;
	        } else {
	            return $this->renderAjax('create', [
	                'model' => $model,
	            ]);
	        }
    	} 
    }

    /**
     * Updates an existing RepliesRecord model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RepliesRecord model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RepliesRecord model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RepliesRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RepliesRecord::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
