<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\users\UsersRecord;
use yii\web\ForbiddenHttpException;
use yii\data\ActiveDataProvider;
use app\models\topics\TopicsRecord;

/**
 * @author nuno
 *
 */
class TopicsApiController extends ActiveController
{
	
	public $modelClass = 'app\models\topics\TopicsRecord';
	public $user;
	
	// TODO: nva DRY - topics and replies api with same functions checkAuth and beforeAction
	
	/*
	 * @inheritdoc
	 */
	public function actions()
	{
		$actions = parent::actions();
	
		/* unset the delete action via the api */
		unset($actions['delete']);
		unset($actions['view']);
		
		return $actions;
	}
	
	
	/**
	 * The action to view a specific topic and list its responses through the api
	 * 
	 * @override
	 * @param integer $topic_id the topic to view 
	 * @return mixed an array containing the topic record and all the ReplyRecords
	 */
	public function actionView($topic_id)
	{
		$model = new $this->modelClass;
		$model->id = $topic_id;
		
		/* @var $model \app\models\topics\TopicsRecord */  
		$topic = new ActiveDataProvider([
            'query' => $model::find()->where(['id' => $topic_id]),
        ]);
		
		$replies =  new ActiveDataProvider([
            'query' => $model->getReplies()->orderBy('created_at DESC'),
        ]);
		
		return ['topic' => $topic->getModels() , 'replies' => $replies->getModels()];
	}
	
	/*
	 * @param string $auth_key the authentication string for the user to access the api
	 * @throws forbiddenhttpexception 
	 * @return boolean if is allowed
	 */
	private function checkAuth($auth_key)
	{
		$this->user = UsersRecord::findIdentityByAccessToken($auth_key);
	
		if( !isset($this->user) )
			throw new ForbiddenHttpException();
	
		return true;
	
	}
	
	/*
	 * @inheritdoc
	 */
	public function beforeAction($action)
	{
		parent::beforeAction($action);
	
		if( $this->checkAuth( Yii::$app->request->get('k')) )
			return true;
	
		return false;
	
	}

}
