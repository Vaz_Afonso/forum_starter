<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\users\UsersRecord;
use yii\web\ForbiddenHttpException;
use Yii;
use app\models\topics\TopicsRecord;

class RepliesApiController extends ActiveController
{

	public $modelClass = 'app\models\replies\RepliesRecord';
	private $user;
	
	/* 
	 * @inheritdoc 
	 */
	public function actions()
	{
		$actions = parent::actions();
		
		/* unset the delete action via the api */
		unset($actions['delete']);
		return $actions;
	}
		
	/*
	 * @param string $auth_key the authentication string for the user to access the api
	 * @return mixed $user the user record
	 */
	public function checkAuth($auth_key)
	{
		$this->user = UsersRecord::findIdentityByAccessToken($auth_key);
	
		if( !isset($this->user) )
			throw new ForbiddenHttpException();
		
		return true;
		
	}
	
	/*
	 * @inheritdoc
	 */
	public function beforeAction($action)
	{
		parent::beforeAction($action);
		
		if( $this->checkAuth( Yii::$app->request->get('k')) )
			return true;
		
		return false;
		
	}
}
