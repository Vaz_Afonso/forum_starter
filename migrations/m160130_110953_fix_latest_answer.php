<?php

use yii\db\Schema;
use yii\db\Migration;

class m160130_110953_fix_latest_answer extends Migration
{
    public function up()
    {
		$this->alterColumn('topics', 'latest_answer', Schema::TYPE_TIMESTAMP .' NULL');
		
    }

    public function down()
    {
    	
        $this->alterColumn('topics', 'latest_answer', Schema::TYPE_TIMESTAMP);
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
