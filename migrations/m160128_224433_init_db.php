<?php

use yii\db\Schema;
use yii\db\Migration;

class m160128_224433_init_db extends Migration
{
    public function up()
    {
    	$this->createTable('users', [
    			'id' => Schema::TYPE_PK . ' NOT NULL',
    			'username' => Schema::TYPE_STRING . '(75) NOT NULL',
    			'password' => Schema::TYPE_STRING . '(60) NOT NULL',
    			'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
    			'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL'
    	],'ENGINE=InnoDB' );
    	
		$this->createTable('topics', [
				'id' => Schema::TYPE_PK . ' NOT NULL',
				'title' => Schema::TYPE_STRING .'(255) NOT NULL',
				'content' => Schema::TYPE_TEXT . ' NOT NULL',
				'latest_answer' => Schema::TYPE_TIMESTAMP,
				'answer_count' => Schema::TYPE_INTEGER,
				'is_api' => Schema::TYPE_BOOLEAN,
				'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL',
				'created_by' => Schema::TYPE_INTEGER . ' NOT NULL'
		],'ENGINE=InnoDB' );
		
		$this->createTable('replies', [
				'id' => Schema::TYPE_PK . ' NOT NULL',
				'title' => Schema::TYPE_STRING .'(255) NOT NULL',
				'content' => Schema::TYPE_TEXT . ' NOT NULL',
				'is_api' => Schema::TYPE_BOOLEAN,
				'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL',
				'topic_id' => Schema::TYPE_INTEGER . ' NOT NULL',
				'created_by' => Schema::TYPE_INTEGER . ' NOT NULL'
		],'ENGINE=InnoDB' );
		
		$this->addForeignKey('fk_reply_topic', 'replies', 'topic_id', 'topics', 'id', 'CASCADE', 'CASCADE');
		
		$this->addForeignKey('fk_reply_user', 'replies', 'created_by', 'users', 'id', 'cascade', 'cascade');
		$this->addForeignKey('fk_topic_user', 'topics', 'created_by', 'users', 'id', 'cascade', 'cascade');
		
    }

    public function down()
    {
    	$this->dropForeignKey('fk_topic_user', 'topics');
    	
        $this->dropForeignKey('fk_reply_user', 'replies');
        $this->dropForeignKey('fk_reply_topic', 'replies');
        
        $this->dropTable('replies');
        $this->dropTable('topics');
        $this->dropTable('users');
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
